FROM continuumio/miniconda3:4.6.14

RUN conda install -y -c conda-forge numpy matplotlib

COPY src /hello_plus_application

