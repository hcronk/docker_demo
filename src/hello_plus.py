import numpy as np
import matplotlib as mpl
mpl.use("agg")
import matplotlib.pyplot as plt

# Compute the x and y coordinates for points on a sine curve 
x = np.arange(0, 3 * np.pi, 0.1) 
y = np.sin(x) 
plt.title("sine wave") 

print("Plotting a sine wave")

# Plot the points using matplotlib 
fig = plt.figure()
plt.plot(x, y)
fig.savefig("sine_wave.png")

